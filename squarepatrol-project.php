<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>SquarePatrol | Software Developer</title>

    <?php include "links.php"; ?>

  </head>
  <body id="project">
    <section class="container-fluid">
      <?php include "_/components/php/header.php"; ?>
      <div class="content row">
          <section class="main col col-lg-8">
            <h2 class="text-center">SquarePatrol - Unity3D</h2>
             <img class="img-responsive" src="images/squarepatrol/scene_cover.png" alt="...">
             <h3>Description</h3>
            <p>Square Patrol is a game built in Unity3D game engine. It has sounds, animation, and multiple levels. It is time based game. You need to reach the green goal before running out of time. There are also coins, and extra time coins.</p>
            <p>On a technical level it uses a game manager pattern which is used widely in the industry. Having a game manager is very useful and helps avoid DRY (Don't repeat yourself). </p>
            <i class="fa fa-github fa-3x"><a href="https://github.com/fredz0003/SquarePatrol"></i>Github Repo</a>
            <h3>Pictures & Media</h3>
            <div class="modalphotos photogrid clearfix">
              <img src="images/squarepatrol/scene_tn.png">
              <img src="images/squarepatrol/spike01_tn.png">
              <img src="images/squarepatrol/spike02_tn.png">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/GwIhGC3GeTI" frameborder="0" allowfullscreen></iframe>
            <h3>Technologies</h3>
            <ul>
              <li>Unity3D</li>
              <li>Blender</li>
              <li>Git</li>
            </ul>
          </section>
          <section class="sidebar col col-lg-4">
            <?php include "_/components/php/sidebar.php"; ?>
          </section>
        
      </div>
      <?php include "_/components/php/footer-2.php"; ?>
    </section>
    <script src="_/components/js/jquery.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="_/components/js/bootstrapvalidator.js"></script>
    <script src="contact-form/assets/js/contact.js"></script>
    <script src="_/js/bootstrap-min.js"></script>
    <script src="_/js/myscript-min.js"></script>

  </body>
</html>