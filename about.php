<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>About | Software Developer</title>

    <?php include "links.php"; ?>

  </head>
  <body id="about">
    <section class="container-fluid">
      <?php include "_/components/php/header.php"; ?>
      <div class="content row">
          <section class="main col col-lg-8">
            <h2>About</h2>
            <p>Kodaman was created by Fernando to pursue his love for programming and software design. Fernando likes to play games on his off time, and spend time with his family. BBQ's are also a nice getaway in the Texas area.
            </p> 
             <h3>Background</h3>
              <p>Around 2005 he created his first website onboard the USS Donald Cook (DDG-75). It was all basic HMTL, but in the development of the intranet site, he found that most users were very pleased by several features. Most of the compartments onboard did not have a TV, and so seeing the movie schedule on their workspace computer was nice. This way they could plan which movie they would see when their watch was over. There's not much to see or do at sea.</p>
            <p>
              The other big feature was the movie trivia question. People could email the answer and if the answer was correct, they could select a movie of their choice to be played later on.
            </p>
            <h3>Skills</h3>
            <p>Fernando posseses many skills in order to keep up with the ever changing world of technology, below are some of the major skills, and he is always adding on new skills as well.</p>
            <ul>
              <li>Java, Obj-C, C++, Python, C#, Javascript, Kotlin, Swift, and more...</li>
              <li>HTML5, CSS3, LESS</li>
              <li>PHP</li>
              <li>Wordpress, Bootstrap</li>
              <li>Photoshop, Illustrator, </li>
              <li>Debugging, and Testing</li>
              <li>Asset Creation</li>
              <li>Animation in Blender</li>
            </ul>
            <h3>Favorite Tools</h3>
            <ul>
              <li>Sublime Text (Text Editor)</li>
              <li>Sketch for Wireframing, and Mockups</li>
              <li>Codekit, and MAMP for Web Development</li>
              <li>Scrivener for content writing, and story writing.</li>
              <li>Xcode, Intellij IDE (IDE's)</li>
              <li>Paparazzi for demo website screenshots</li>
            </ul>
            <h3>Formal Education</h3>
            <ul>
              <li>Lonestar College Game Programming AAS</li>
              <li>Stanford iOS Development Online Course</li>
              <li>Stanford Android Development Online Course</li>
              <li>Wordpress Themes Online Course</li>
            </ul>
          </section>
          <section class="sidebar col col-lg-4">
            <?php include "_/components/php/sidebar.php"; ?>
          </section>
          
        </div>
      <?php include "_/components/php/footer-2.php"; ?>
      </section>
      
    <script src="_/components/js/jquery.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="_/components/js/bootstrapvalidator.js"></script>
    <script src="contact-form/assets/js/contact.js"></script>
    <script src="_/js/bootstrap-min.js"></script>
    <script src="_/js/myscript-min.js"></script>

  </body>
</html>