<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Kodaman | Software Developer</title>

    <?php include "links.php"; ?>

  </head>
  <body id="contact-page">
    <section class="container-fluid">
          <?php include "_/components/php/header.php"; ?>
          <section class="main col col-lg-8">
            <?php include "_/components/php/widget-contact.php"; ?>
          </section>
          <section class="sidebar col col-lg-4">
            <?php include "_/components/php/sidebar.php"; ?>
          </section>
    </section>
    <?php include "_/components/php/footer-2.php"; ?>
    <script src="_/components/js/jquery.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="_/components/js/bootstrapvalidator.js"></script>
    <script src="contact-form/assets/js/contact.js"></script>
    <script src="_/js/bootstrap-min.js"></script>
    <script src="_/js/myscript-min.js"></script>

  </body>
</html>