	<!-- Bootstrap -->
    <link href="_/css/bootstrap.css" rel="stylesheet">
    <link href="_/css/mystyles.css" rel="stylesheet">
    <link href="_/css/footer.css" rel="stylesheet">
    <link href="_/css/circle.css" rel="stylesheet">
    <!-- Font Awesome 4.7 -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Ubuntu+Mono" rel="stylesheet">
    <link rel="icon" href="images/favicon-black.png">