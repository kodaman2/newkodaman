<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Kodaman | Software Developer</title>

    <?php include "links.php"; ?>

  </head>
  <body>
    <section class="container-fluid">
      <div class="content row">
          <?php include "_/components/php/header.php"; ?>
          <section class="main col col-lg-8">
            <p>Lorem ipsum dolor sit met, consectetur adipiscing elit. Mauris vestibul nisl at arcu porttutor feugiat. Nunc egestas dignissim nisi quis elementum. Aliquam nisi urna, mattis ac eleifend vehicle, lacinia a mi. Quisque euismod mauri ex, ut ullamcorper enim trirtique non. Dus eleifend et tortuur vestibule iaculis. Pellentesque ligula ante, condimentum sed geestas vehicula, congé eget eros. Nullam viverra leo nisi, id consectetuer ligula imperdiet id.</p>
          </section>
          <section class="sidebar col col-lg-4">
            <p>Lorem ipsum dolor sit met, consectetur adipiscing elit. Mauris vestibul nisl at arcu porttutor feugiat. Nunc egestas dignissim nisi quis elementum.</p>
          </section>
        
        </div>
      <?php include "_/components/php/footer-2.php"; ?>
    </section>
    <script src="_/components/js/jquery.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="_/components/js/bootstrapvalidator.js"></script>
    <script src="contact-form/assets/js/contact.js"></script>
    <script src="_/js/bootstrap-min.js"></script>
    <script src="_/js/myscript-min.js"></script>

  </body>
</html>