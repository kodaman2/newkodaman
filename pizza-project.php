<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Kodaman | Software Developer</title>

    <?php include "links.php"; ?>

  </head>
  <body id="project">
    <section class="container-fluid">
      <?php include "_/components/php/header.php"; ?>
      <div class="content row">
          <section class="main col col-lg-8">
            <h2 class="text-center">UX Mobile Pizza App</h2>
             <img class="img-responsive" src="images/uxpizza/pizza-cover2.png" alt="...">
             <h3>Description</h3>
            <p>The UX (User Experience) Flow was created to solve one problem. It is clear they are losing money on mobile and online sales. After some research I found out the real problem. Many customers like the convenience of ordering while at home, and many apps provide these simple solution. The app only caters to franchise owners that have an expensive POS (Point of Sale) system, while the ones that did not purchase the POS system do not have any advantages and they are not even listed under the app.</p>
            <p>The solution is quite easy. It is needed to setup a database of all the stores, and one of the main properties will be to check if it posses a POS system, browser payment setup (By owner), or other payment discussed on the brief. We will also have to handle the browser payment setup on the backend by adding some extra pages to the existing website. The owner would request a secret key by email, and then authenticate on the site and setup the payment. It can be the owners business account, or paypal, or any option for that matter. The existing owners with the current POS will not be affected by the new system, since we are only focusing on adding the stores that were left out for mobile ordering in the beginning.</p>
            <h3>Secure Payments</h3>
            <p>
              The key when handling payments online or mobile apps is security. There are many payment gateways taht allow credit card transation data thorugh their API's. Doing it this way might not be the best solution since the data passes through the mobile app. Furthermore, not all payment gateways support an autenticated tokenization process, which is required when accepting payments from a mobile app. 
            </p>
            <p>
              Major gateways or payment systems such as Stripe and Paypal's Braintree offer robust native mobile libraries for Android and iOS and ease PCI compliance by sending encrypted credit card data as a token. 
            </p>
            <h3>Files</h3>
            <a href="images/uxpizza/PizzaUX.pdf">View PDF</a>
            <h3>Pictures</h3>
            <div class="modalphotos photogrid clearfix">
              <img src="images/uxpizza/home-tn.png">
              <img src="images/uxpizza/signin-tn.png">
               <img src="images/uxpizza/menu-tn.png">
            <h3>Technologies Summary</h3>
            <ul>
              <li>Sketch</li>
              <li>Photoshop</li>
            </ul>
          </section>
          <section class="sidebar col col-lg-4">
            <?php include "_/components/php/sidebar.php"; ?>
          </section>
        
      </div>
      <?php include "_/components/php/footer-2.php"; ?>
    </section>
    <script src="_/components/js/jquery.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="_/components/js/bootstrapvalidator.js"></script>
    <script src="contact-form/assets/js/contact.js"></script>
    <script src="_/js/bootstrap-min.js"></script>
    <script src="_/js/myscript-min.js"></script>

  </body>
</html>