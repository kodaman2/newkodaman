<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Kodaman | Software Developer</title>

    <?php include "links.php"; ?>

  </head>
  <body id="portfolio-page">
    <section class="container-fluid">
      <?php include "_/components/php/header.php"; ?>
      <div class="content row">
          <section class="main col col-lg-8 col-md-10 col-xs-10">

            <div class="btn-group" id="filterOptions" role="group" aria-label="...">
              <button type="button" class="btn btn-default active" id="all">All</button>
              <button type="button" class="btn btn-default" id="web-item">Web</button>
              <button type="button" class="btn btn-default" id="mobile-item">Apps</button>
              <button type="button" class="btn btn-default" id="game-item">Games</button>
            </div>

            <div id="ourHolder">
               <?php include "_/components/php/portfolio/portfolio-roux-tn.php"; ?>
                <?php include "_/components/php/portfolio/portfolio-squarepatrol-tn.php"; ?>
                <?php include "_/components/php/portfolio/portfolio-pizzaux-tn.php"; ?>
            </div>

          </section>
          <section class="sidebar col col-lg-4 col-md-6 col-xs-6">
            <?php include "_/components/php/sidebar.php"; ?>
        </section>
        </div>
      <?php include "_/components/php/footer-2.php"; ?>
    
    <script src="_/components/js/jquery.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="_/components/js/bootstrapvalidator.js"></script>
    <script src="contact-form/assets/js/contact.js"></script>
    <script src="_/js/bootstrap-min.js"></script>
    <script src="_/js/myscript-min.js"></script>

  </body>
</html>