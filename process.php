<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Process | Software Developer</title>

    <!-- Bootstrap -->
    <link href="_/css/bootstrap.css" rel="stylesheet">
    <link href="_/css/mystyles.css" rel="stylesheet">
    <link href="_/css/footer.css" rel="stylesheet">
    <!-- Font Awesome 4.7 -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

  </head>
  <body>
    <section class="container-fluid">
       <?php include "_/components/php/header.php"; ?>
      <div class="content row">
          <section class="main col col-lg-8">
            <h2>Creative Process</h2>
            <p>I always like sharing the creative process, it shows what it takes to create a website. The process is quite simple, like many other subjects it requires good planing. Everything starts with the specifications for the website. This could be in a form, or one to one conversation with the customer. Below are some example of questions to get an idea of the project.</p>

            <ul>
              <li>What does your business actually do?</li>
              <li>What do you want your site to accomplish?</li>
              <li>Do you have a website already?</li>
              <li>What makes your company remarkable?</li>
              <li>Who are your competitors?</li>
              <li>What websites do you like and why?</li>
              <li>Who are your customers and what are their pains?</li>
              <li>What features do you want your website to have?</li>
              <li>How will you record your results?</li>
              <li>Do you have any style guide or existing collateral?</li>
            </ul>

            <p>Research</p>
            
          </section>
          <section class="sidebar col col-lg-4">
            <?php include "_/components/php/sidebar.php"; ?>
          </section>
        
      </div>
      <?php include "_/components/php/footer-2.php"; ?>
    </section>
    <script src="_/components/js/jquery.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="_/components/js/bootstrapvalidator.js"></script>
    <script src="contact-form/assets/js/contact.js"></script>
    <script src="_/js/bootstrap-min.js"></script>
    <script src="_/js/myscript-min.js"></script>

  </body>
</html>