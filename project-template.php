<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Kodaman | Software Developer</title>

    <?php include "links.php"; ?>

  </head>
  <body id="project">
    <section class="container-fluid">
       <?php include "_/components/php/header.php"; ?>
      <div class="content row">
          <section class="main col col-lg-8">
            <h2 class="text-center">Project Title</h2>
             <img class="img-responsive" src="images/acrilycs_workshop.jpg" alt="...">
             <h3>Description</h3>
            <p>Lorem ipsum dolor sit met, consectetur adipiscing elit. Mauris vestibul nisl at arcu porttutor feugiat. Nunc egestas dignissim nisi quis elementum. Aliquam nisi urna, mattis ac eleifend vehicle, lacinia a mi. Quisque euismod mauri ex, ut ullamcorper enim trirtique non. Dus eleifend et tortuur vestibule iaculis. Pellentesque ligula ante, condimentum sed geestas vehicula, congé eget eros. Nullam viverra leo nisi, id consectetuer ligula imperdiet id.</p>
            <h3>Pictures</h3>
            <div class="modalphotos photogrid clearfix">
              <img src="images/artist_on_table_tn.jpg">
              <img src="images/artist_on_table_tn.jpg">
              <img src="images/artist_on_table_tn.jpg">
            <h3>Creative Process</h3>
            <h3>Technologies Summary</h3>
          </section>
          <section class="sidebar col col-lg-4">
            <?php include "_/components/php/sidebar.php"; ?>
          </section>
        
      </div>
      <?php include "_/components/php/footer-2.php"; ?>
    </section>
    <script src="_/components/js/jquery.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="_/components/js/bootstrapvalidator.js"></script>
    <script src="contact-form/assets/js/contact.js"></script>
    <script src="_/js/bootstrap-min.js"></script>
    <script src="_/js/myscript-min.js"></script>

  </body>
</html>