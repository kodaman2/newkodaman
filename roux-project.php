<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Roux Project | Software Developer</title>

    <?php include "links.php"; ?>

  </head>
  <body id="project">
    <section class="container-fluid">
       <?php include "_/components/php/header.php"; ?>
      <div class="content row">
          <section class="main col col-lg-8">
            <h2>Roux Academy Conference</h2>
             <img class="img-responsive" src="images/roux/Roux-Home_Cover.jpg" alt="...">
             <h3 >Description</h3>
            <p>
              This conference project was fun to code. Bootstrap is doing some heavy lifting in the background. A fully functional carousel can be built in matter of minutes with just a bit of HTML, and JS. All in all the site contains must of the features of bootstrap. Modals, carousel, tabs, scrollspy, just to name a few. The ease of the responsive bootstrap grid system is amazing. 
            </p>
            <p>
              Another good feature of this site is how it was built with modularity. It is easy to make a change on the footer without having to make a change in 10 page. The responsive design give us great user experience across desktop, mobile, and table devices.
            </p>
            <!-- <h3>Pictures</h3> -->
            <div class="modalphotos photogrid clearfix">
              <img src="images/roux/Roux-Home_tn.jpg">
              <img src="images/roux/Roux-Venue_tn.jpg">
              <img src="images/roux/Roux-Artists_tn.jpg">
              <img src="images/roux/Roux-Schedule_tn.jpg">
              <img src="images/roux/roux.dev-home(iPhone6)_tn.png">
              <img src="images/roux/roux.dev-(iPad)_tn.png">
            <h3>Creative Process</h3>
            <p>For this project it was essential to have several features. The main feature of the site was a way to connect the artists with the attendees. Clearly some of the most important items was to showcase the artists, show the schedule, and have a nice way to register, to know more about the conference.</p>
            <p>Another must have feature was to show the venue. Since ateendes were coming from all over the country, it was essential to show the venues. This gave a clear idea of what to expect in terms of locations, and help attendees plan their day.</p>
            <p>In tech terms one way to showcase the artwork was by using thubmnails in a grid, and a modal popover. It was imperative not to diminish the quality of the artwork photos. The thubmnail is a small size picture (160px by 160px), and the popover a high resoution picture. The scrollspy was another good bootstrap feature, and assists in jumping to the artist you want to check without having to scroll. The schedule and register pages were mainly to assist attendees with their planning. The schedule page shows all five days, and which artist will be showcasing in that particular day, along with which venue and time.</p>
            <!-- <p><a href="process.php" class="btn btn-primary" role="button">Learn more</a></p> -->
            <h3>Technologies</h3>
            <ul>
              <li>HMTL5, CSS3, LESS, & Javascript</li>
              <li>Bootstrap Framework</li>
              <li>Photoshop</li>
              <li>Codekit</li>
            </ul>
          </section>
          <section class="sidebar col col-lg-4">
            <?php include "_/components/php/sidebar.php"; ?>
          </section>
        
      </div>
      <?php include "_/components/php/footer-2.php"; ?>
    </section>
    <script src="_/components/js/jquery.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="_/components/js/bootstrapvalidator.js"></script>
    <script src="contact-form/assets/js/contact.js"></script>
    <script src="_/js/bootstrap-min.js"></script>
    <script src="_/js/myscript-min.js"></script>

  </body>
</html>