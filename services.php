<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>Services | Software Developer</title>

    <!-- Bootstrap -->
    <link href="_/css/bootstrap.css" rel="stylesheet">
    <link href="_/css/mystyles.css" rel="stylesheet">
    <link href="_/css/footer.css" rel="stylesheet">
    <!-- Font Awesome 4.7 -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

  </head>
  <body>
    <section class="container-fluid">
      <?php include "_/components/php/header.php"; ?>
      <div class="content row">
          <section class="main col col-lg-8">
            <h2>Services</h2>
            <p>Lorem ipsum dolor sit met, consectetur adipiscing elit. Mauris vestibul nisl at arcu porttutor feugiat. Nunc egestas dignissim nisi quis elementum. Aliquam nisi urna, mattis ac eleifend vehicle, lacinia a mi. Quisque euismod mauri ex, ut ullamcorper enim trirtique non. Dus eleifend et tortuur vestibule iaculis. Pellentesque ligula ante, condimentum sed geestas vehicula, congé eget eros. Nullam viverra leo nisi, id consectetuer ligula imperdiet id.</p>
            <p>Lorem ipsum dolor sit met, consectetur adipiscing elit. Mauris vestibul nisl at arcu porttutor feugiat. Nunc egestas dignissim nisi quis elementum. Aliquam nisi urna, mattis ac eleifend vehicle, lacinia a mi. Quisque euismod mauri ex, ut ullamcorper enim trirtique non. Dus eleifend et tortuur vestibule iaculis. Pellentesque ligula ante, condimentum sed geestas vehicula, congé eget eros. Nullam viverra leo nisi, id consectetuer ligula imperdiet id.</p>
            <p>Lorem ipsum dolor sit met, consectetur adipiscing elit. Mauris vestibul nisl at arcu porttutor feugiat. Nunc egestas dignissim nisi quis elementum. Aliquam nisi urna, mattis ac eleifend vehicle, lacinia a mi. Quisque euismod mauri ex, ut ullamcorper enim trirtique non. Dus eleifend et tortuur vestibule iaculis. Pellentesque ligula ante, condimentum sed geestas vehicula, congé eget eros. Nullam viverra leo nisi, id consectetuer ligula imperdiet id.</p>
            <p>Lorem ipsum dolor sit met, consectetur adipiscing elit. Mauris vestibul nisl at arcu porttutor feugiat. Nunc egestas dignissim nisi quis elementum. Aliquam nisi urna, mattis ac eleifend vehicle, lacinia a mi. Quisque euismod mauri ex, ut ullamcorper enim trirtique non. Dus eleifend et tortuur vestibule iaculis. Pellentesque ligula ante, condimentum sed geestas vehicula, congé eget eros. Nullam viverra leo nisi, id consectetuer ligula imperdiet id.</p>
            <p>Lorem ipsum dolor sit met, consectetur adipiscing elit. Mauris vestibul nisl at arcu porttutor feugiat. Nunc egestas dignissim nisi quis elementum. Aliquam nisi urna, mattis ac eleifend vehicle, lacinia a mi. Quisque euismod mauri ex, ut ullamcorper enim trirtique non. Dus eleifend et tortuur vestibule iaculis. Pellentesque ligula ante, condimentum sed geestas vehicula, congé eget eros. Nullam viverra leo nisi, id consectetuer ligula imperdiet id.</p>
            <p>Lorem ipsum dolor sit met, consectetur adipiscing elit. Mauris vestibul nisl at arcu porttutor feugiat. Nunc egestas dignissim nisi quis elementum. Aliquam nisi urna, mattis ac eleifend vehicle, lacinia a mi. Quisque euismod mauri ex, ut ullamcorper enim trirtique non. Dus eleifend et tortuur vestibule iaculis. Pellentesque ligula ante, condimentum sed geestas vehicula, congé eget eros. Nullam viverra leo nisi, id consectetuer ligula imperdiet id.</p>
          </section>
          <section class="sidebar col col-lg-4">
            <?php include "_/components/php/sidebar.php"; ?>
          </section>
          
        </div>
      <?php include "_/components/php/footer-2.php"; ?>
      </section>
      
    <script src="_/components/js/jquery.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script src="_/components/js/bootstrapvalidator.js"></script>
    <script src="contact-form/assets/js/contact.js"></script>
    <script src="_/js/bootstrap-min.js"></script>
    <script src="_/js/myscript-min.js"></script>

  </body>
</html>