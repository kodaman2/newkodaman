$(function() {

    // highlight the current nav
    /*
    * Target the page id, i.e. #home
    * Then look for anchor tag that cotaints specific word
    * lastly add active class
    */
    $("#home-page a:contains('Home')").parent().addClass("active");
    $("#about a:contains('About')").parent().addClass("active");
    $("#contact-page a:contains('Contact')").parent().addClass("active");
    $("#portfolio-page a:contains('Portfolio')").parent().addClass("active");

    // Dropdown menu filters content i.e. games, apps, web
    $(document).ready(function() {
        $('#filterOptions button').click(function() {
            // fetch the class of the clicked item
            var ourClass = $(this).attr('id');
            //console.log("Button clicked! " + ourClass);
            // reset the active class on all the buttons
            $('#filterOptions button').removeClass('active');
            // update the active state on our clicked button
            $(this).addClass('active');

            if (ourClass == 'all') {
                // show all our items
                $('#ourHolder').children('div.item').show(); 
            } else {
                // hide all elements that don't share ourClass
                $('#ourHolder').children('div:not(.' + ourClass + ')').hide();
                // show all elements that do share ourClass
                $('#ourHolder').children('div.' + ourClass).show();
            }
            return false;
        });
    });

    $(document).ready(function() {
        $('#contact_form').bootstrapValidator({
                // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    first_name: {
                        validators: {
                            stringLength: {
                                min: 2,
                            },
                            regexp: {
                                regexp: /^[a-zA-Z]*$/,
                                message: 'Can only contain letters, and no whitespace'
                            },
                            notEmpty: {
                                message: 'Please supply your first name'
                            }
                        }
                    },
                    last_name: {
                        validators: {
                            stringLength: {
                                min: 2,
                            },
                            regexp: {
                                regexp: /^[a-zA-Z]*$/,
                                message: 'Can only contain letters, and no whitespace'
                            },
                            notEmpty: {
                                message: 'Please supply your last name'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Please supply your email address'
                            },
                            emailAddress: {
                                message: 'Please supply a valid email address'
                            }
                        }
                    },

                    phone: {
                        validators: {
                            stringLength: {
                                min: 2,
                            },
                            regexp: {
                                regexp: /^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i,
                                message: 'Example 713-555-2323 or (713) 555-3434'
                            }
                        }
                    },

                    skype: {
                        validators: {
                            stringLength: {
                                min: 2,
                            },
                            regexp: {
                                regexp: /^[a-z0-9_.-]{3,15}$/,
                                message: 'The username can contain letters, numbers, dot, underscore or hyphen'
                            }
                        }
                    },

                    city: {
                        validators: {
                            stringLength: {
                                min: 4,
                            },
                            regexp: {
                                regexp: /^[a-zA-Z ]*$/,
                                message: 'Can only contain letters, and whitespace'
                            }
                            // notEmpty: {
                            //     message: 'Please supply your city'
                            // }
                        }
                    },
                    state: {
                        validators: {
                            notEmpty: {
                                message: 'Please select your state, or other'
                            }
                        }
                    },

                    url: {
                        validators: {
                            uri: {
                                allowLocal: true,
                                message: 'Plese enter a valid URL, example http://www.example.com'
                            }
                        }
                    },

                    comment: {
                        validators: {
                            stringLength: {
                                min: 10,
                                max: 200,
                                message: 'Please enter at least 10 characters and no more than 200'
                            },
                            notEmpty: {
                                message: 'Please supply a description of your project'
                            }
                        }
                    }
                }
            })
            .on('success.form.bv', function(e) {
                // $('#success_message').slideDown({
                //         opacity: "show"
                //    }, "slow") // Do something ...

                // remove errors
                $('.form-control').removeClass('has-error'); // remove the error class
                $('.help-block').remove(); // remove the error text

                $('#contact_form').data('bootstrapValidator').resetForm();

                // Prevent form submission
                e.preventDefault();

                // Get the form instance
                var $form = $(e.target);

                // Get the BootstrapValidator instance
                var bv = $form.data('bootstrapValidator');

                // Use Ajax to submit form data
                //$.post($form.attr('action'), $form.serialize(), function(result) {
                //console.log(result);
                //}, 'json');

                // get form data
                // BUG this data is not properly formated as JSON!
                var formData = {
                    'firstname': $("input[name='first_name']").val(),
                    'lastname': $("input[name='last_name']").val(),
                    // 'email': $("input[name='email']").val(),
                    // 'phone': $("input[name='phone']").val(),
                    // 'skype': $("input[name='skype']").val(),
                    // 'city': $("input[name='city']").val(),
                    // 'state': $("input[name='state']").val(),
                    // 'url': $("input[name='url']").val(),
                    // 'hosting': $("input[name='hosting']:checked").val(),
                    // 'comment': $("input[name='comment']").val()
                };


                // process the form with php
                $.ajax({
                    type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                    url: '_/components/php/contact/contact-process2.php', // the url where we want to POST
                    data: formData, // our data object
                    dataType: 'json', // what type of data do we expect back from the server
                    encode: true
                })

                // using the done promise callback
                .done(function(data) {

                    // log data to the console so we can see
                    console.log(data);

                    // here we will handle errors and validation messages
                    if (!data.success) {

                        console.log(data.success);
                        //console.log(data.errors);

                        $('#error_message').show('fade');

                        setTimeout(function() {
                            $('#error_message').hide('fade');
                        }, 4000);

                        // handle errors for name ---------------
                        if (data.errors.name) {
                            $('#name-group').addClass('has-error'); // add the error class to show red input
                            $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                        }

                        // handle errors for email ---------------
                        if (data.errors.email) {
                            $('#email-group').addClass('has-error'); // add the error class to show red input
                            $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                        }

                        // handle errors for skype ---------------
                        if (data.errors.skype) {
                            $('#skype-group').addClass('has-error'); // add the error class to show red input
                            $('#skype-group').append('<div class="help-block">' + data.errors.skype + '</div>'); // add the actual error message under our input
                        }



                    } else {
                        // ALL GOOD! just show the success message!
                        //$('form').append('<div class="alert alert-success">' + data.message + '</div>');

                        // usually after form submission, you'll want to redirect
                        // window.location = '/thank-you'; // redirect a user to another page
                        //alert('success'); // for now we'll just alert the user
                        //console.log(data.success);
                        $('#success_message').show('fade');

                        setTimeout(function() {
                            $('#success_message').hide('fade');
                        }, 4000);

                    }

                })

                .fail(function(jqXHR, textStatus, errorThrown) {
                    // show any errors
                    // remove for production
                    //console.log(data);
                    console.log(errorThrown);
                    console.warn(jqXHR.responseText);

                    console.log("Failing error");

                });

                // stop the form from submitting the normal way and refreshing the page
                //event.preventDefault();
            });

    });




    // magic.js
    $(document).ready(function() {

        // process the form
        $('#ajax-form').submit(function(event) {

            // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();

            $('.form-group').removeClass('has-error'); // remove the error class
            $('.help-block').remove(); // remove the error text

            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'name': $('input[name=name]').val(),
                'email': $('input[name=email]').val(),
                'superheroAlias': $('input[name=superheroAlias]').val()
            };

            // process the form
            $.ajax({
                    type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                    url: '_/components/php/contact/process.php', // the url where we want to POST
                    data: formData, // our data object
                    dataType: 'json', // what type of data do we expect back from the server
                    encode: true
                })
                // using the done promise callback
                .done(function(data) {

                    // log data to the console so we can see
                    console.log(data);

                    // here we will handle errors and validation messages
                    if (!data.success) {

                        console.log('Error');

                        $('#error_message').show('fade');

                        setTimeout(function() {
                            $('#error_message').hide('fade');
                        }, 6000);

                        // handle errors for name ---------------
                        if (data.errors.name) {
                            $('#name-group').addClass('has-error'); // add the error class to show red input
                            $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                        }

                        // handle errors for email ---------------
                        if (data.errors.email) {
                            $('#email-group').addClass('has-error'); // add the error class to show red input
                            $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                        }


                    } else {

                        // ALL GOOD! just show the success message!
                        //$('form').append('<div class="alert alert-success">' + data.message + '</div>');
                        $('#success_message').show('fade');

                        setTimeout(function() {
                            $('#success_message').hide('fade');
                        }, 6000);

                        // usually after form submission, you'll want to redirect
                        // window.location = '/thank-you'; // redirect a user to another page
                        //alert('success'); // for now we'll just alert the user
                        console.log("Form submitted");

                    }

                })

            .fail(function(data) {
                // show any errors
                // remove for production
                console.log(data);
                console.log("Fail. An error has occurred")
            });


        });

    });

    // show modals photos
    // selector: period is to find a class, and then target any img
    // on click, and then literal function
    // pound sign is to find id's
    $('.modalphotos img').on('click', function() {
        $('#modal').modal({
            show: true,
        })

        // Need to check if is png or jpg
        if (this.src.lastIndexOf('.jpg') + 1) {
            var mySource = this.src.substr(0, this.src.length - 7) + '.jpg';
        }
        if (this.src.lastIndexOf('.png') + 1) {
            var mySource = this.src.substr(0, this.src.length - 7) + '.png';
        } else {
            console.log("Images need to be jpg or png for modal to work");
        }

        //var mySource = this.src.substr(0, this.src.length-7) +  '.jpg';
        $('#modalimage').attr('src', mySource);
        $('#modalimage').on('click', function() {
            $('#modal').modal('hide');
        })

    });

}); // Load Jquery