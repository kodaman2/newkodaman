<div class="row content">
  <footer class="footer-distributed">
    <div class="footer-left">
      <!-- <h3>Kodaman</h3> -->
      <img src="images/logo-full-white.png" alt="logo-img">
      <p class="footer-links">
        <a href="index.php">Home</a>
        <!-- ·
        <a href="#">Blog</a> -->
        <!-- · 
        <a href="#">Pricing</a> -->
        ·
        <a href="about.php">About</a>
        <!-- ·
        <a href="#">Faq</a> -->
        ·
        <a href="contact.php">Contact</a>
      </p>
      <p class="footer-company-name">Kodaman &copy; <?php echo date("Y");?></p>
    </div>
    <div class="footer-center">
      <div>
        <i class="fa fa-map-marker"></i>
        <p><span></span> Houston, Texas</p>
      </div>
      <div>
        <i class="fa fa-envelope"></i>
        <p><a href="mailto:sales@kodaman.tech">sales@kodaman.tech</a></p>
      </div>
    </div>
    <div class="footer-right">
      <p class="footer-company-about">
        <span>About the company</span>
        Founded by Fernando Balandran. A creative company providing solutions in the media industry.
      </p>
      <div class="footer-icons">
        <a href="https://www.facebook.com/BalandranF"><i class="fa fa-facebook"></i></a>
        <a href="https://twitter.com/kodamanfb"><i class="fa fa-twitter"></i></a>
        <a href="https://www.linkedin.com/in/fernando-balandran-85b85897/"><i class="fa fa-linkedin"></i></a>
       <a href="https://github.com/fredz0003"><i class="fa fa-github"></i></a>
        <a href="https://www.youtube.com/channel/UCIFRJz8p3ZBKAYmmAqnGueg"><i class="fa fa-youtube"></i></a>
        <a href="https://www.behance.net/ic3balandr4b1a"><i class="fa fa-behance-square"></i></a>
        <!-- <a href="#"><i class="fa fa-instagram"></i></a> -->
      </div>
    </div>
  </footer>
</div>