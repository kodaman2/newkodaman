<div class="row content" id="services">
  <h2 class="text-center">What I do</h2>

  <div class="col-sm-6 col-md-4">
        <div class="services-icons-div">
          <i class="fa fa-html5 fa-3x"></i>
          <i class="fa fa-css3 fa-3x"></i>
          <i class="fa fa-wordpress fa-3x"></i>
          <i class="fa fa-chrome fa-3x"></i>
        </div>
        <div class="caption">
          <h3 >Web Development</h3>
          <ul>
            <li>Wordpress Customizations, and Themes</li>
            <li>Websites from concepts to deployment</li>
            <li>Add-ons</li>
            <li>Bug</li>
            <li>Wordpress, Bootstrap, HMLT5, CSS3, Javascript, PHP, MySQL, Firebase, Chrome Dev tools.</li>
          </ul>
        </div>
  </div>

  <div class="col-sm-6 col-md-4">
        <div class="services-icons-div">
          <i class="fa fa-gamepad fa-3x"></i>
          <i class="fa fa-trophy fa-3x"></i>
          <i class="fa fa-bolt fa-3x"></i>
          <i class="fa fa-bomb fa-3x"></i>
        </div>
        
        <div class="caption">
          <h3>Game Development</h3>
          <ul>
            <li>Mobile games from concepts to game design document</li>
            <li>Animations</li>
            <li>Scripting</li>
            <li>Game testing</li>
            <li>Unity3d, C#, Blender, Illustrator</li>
          </ul>
        </div>
  </div>

  <div class="col-sm-6 col-md-4">
        <div class="services-icons-div">
          <i class="fa fa-apple fa-3x"></i>
          <i class="fa fa-android fa-3x"></i>
          <i class="fa fa-bug fa-3x"></i>
          <i class="fa fa-github fa-3x"></i>
        </div>
        
        <div class="caption">
          <h3>App Development</h3>
          <ul>
            <li>iOS Mobile Applications</li>
            <li>Android Mobile Applications</li>
            <li>App testing</li>
            <li>Add-on</li>
            <li>Bugs</li>
            <li>Xcode, Objective-C, Swift, IDEA, Java, Kotlin, GIT.</li>
          </ul>
        </div>
  </div>
</div>