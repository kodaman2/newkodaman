<?php include "contact/contact-process.php"; ?>

<div class="row" id="contact">
    <form class="well form-horizontal" action="_/components/php/contact/contact-process2.php" method="post"  id="contact_form">
      <fieldset>
        <!-- Form Name -->
        <legend>Have a project in mind. Let us help you kickstart it, contact us today! We also love any feedback.</legend>

        <div class="form-group">
          <label class="col-md-4 control-label">First Name</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input name="first_name" placeholder="First Name" class="form-control"  type="text"> 
            </div>
            <span class="error"></span>
          </div>
        </div>
        <!-- Fist name-->

        <div class="form-group">
          <label class="col-md-4 control-label" >Last Name</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
              <input name="last_name" placeholder="Last Name" class="form-control"  type="text">
            </div>
             <span class="error"></span>
          </div>
        </div>
        <!-- Last name-->

        <div class="form-group">
          <label class="col-md-4 control-label">E-Mail</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
              <input name="email" placeholder="E-Mail Address" class="form-control"  type="text">
            </div>
             <span class="error"></span>
          </div>
        </div>
        <!-- Email -->
        
        <div class="form-group">
          <label class="col-md-4 control-label">Phone #</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
              <input name="phone" placeholder="(845) 555-1212" class="form-control" type="text">
            </div>
          </div>
        </div>
        <!-- Phone-->

        <div id="skype-group" class="form-group">
          <label class="col-md-4 control-label">Skype</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
              <input name="skype" placeholder="User ID" class="form-control" type="text">
            </div>
          </div>
        </div>
        <!-- Skype -->
        
        <div class="form-group">
          <label class="col-md-4 control-label">City</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
              <input name="city" placeholder="city" class="form-control"  type="text">
            </div>
          </div>
        </div>
        <!-- City -->
        
        <div class="form-group">
          <label class="col-md-4 control-label">State</label>
          <div class="col-md-4 selectContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
              <select name="state" class="form-control selectpicker" >
                <option value=" " >Please select your state</option>
                <option>Alabama</option>
                <option>Alaska</option>
                <option >Arizona</option>
                <option >Arkansas</option>
                <option >California</option>
                <option >Colorado</option>
                <option >Connecticut</option>
                <option >Delaware</option>
                <option >District of Columbia</option>
                <option> Florida</option>
                <option >Georgia</option>
                <option >Hawaii</option>
                <option >daho</option>
                <option >Illinois</option>
                <option >Indiana</option>
                <option >Iowa</option>
                <option> Kansas</option>
                <option >Kentucky</option>
                <option >Louisiana</option>
                <option>Maine</option>
                <option >Maryland</option>
                <option> Mass</option>
                <option >Michigan</option>
                <option >Minnesota</option>
                <option>Mississippi</option>
                <option>Missouri</option>
                <option>Montana</option>
                <option>Nebraska</option>
                <option>Nevada</option>
                <option>New Hampshire</option>
                <option>New Jersey</option>
                <option>New Mexico</option>
                <option>New York</option>
                <option>North Carolina</option>
                <option>North Dakota</option>
                <option>Ohio</option>
                <option>Oklahoma</option>
                <option>Oregon</option>
                <option>Other</option>
                <option>Pennsylvania</option>
                <option>Rhode Island</option>
                <option>South Carolina</option>
                <option>South Dakota</option>
                <option>Tennessee</option>
                <option>Texas</option>
                <option> Uttah</option>
                <option>Vermont</option>
                <option>Virginia</option>
                <option >Washington</option>
                <option >West Virginia</option>
                <option>Wisconsin</option>
                <option >Wyoming</option>
              </select>
            </div>
          </div>
        </div>
        <!-- State -->
  
        <div class="form-group">
          <label class="col-md-4 control-label">Website or domain name</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
              <input name="url" placeholder="Website or domain name" class="form-control" type="text">
            </div>
            <span class="error"></span>
          </div>
        </div>
        <!-- Website or Domain name -->

        <div class="form-group">
          <label class="col-md-4 control-label">Do you have hosting?</label>
          <div class="col-md-4">
            <div class="radio">
              <label>
                <input type="radio" name="hosting" value="yes" /> Yes
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="hosting" value="no" /> No
              </label>
            </div>
          </div>
        </div>
        <!-- Hosting radio button -->
        
        <div class="form-group">
          <label class="col-md-4 control-label">Project Description</label>
          <div class="col-md-4 inputGroupContainer">
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
              <textarea class="form-control" type="text" name="comment" placeholder="Project Description"></textarea>
            </div>
          </div>
        </div>
        <!-- Project description or Message -->

        <!-- Success message -->
        <div class="alert alert-success" role="alert" id="success_message">Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks for contacting us, we will get back to you shortly.</div>
        <!-- Error message  -->
        <div class="alert alert-danger" role="alert" id="error_message">Error <i class="glyphicon glyphicon-exclamation-sign"></i> Please fix the form errors.</div>
        <!-- Button -->
        <div class="form-group">
          <label class="col-md-4 control-label"></label>
          <div class="col-md-4">
            <button name="submit" id="submit" type="submit" class="btn btn-warning" data-submit="...Sending" >Send <span class="glyphicon glyphicon-send"></span></button>
          </div>
        </div>
      </fieldset>
    </form>
  </div>
</div>
<!-- row -->