<div class="row content" id="widget-contact">
	<div class="container-fluid">
		<h2 class="text-center">Contact Form</h2>
		<form action="contact-form/process.php" enctype="multipart/form-data" method="POST" name="contact" id="contact" class="ucf form-horizontal" novalidate>
			<div class="message"></div>
			<fieldset>
				<legend class="text-center">Have a project in mind. Let us help you kickstart it, contact us today! We also love any feedback.</legend>
				

		        <div class="form-group">
		          <label class="col-md-3 control-label">Name *</label>
		          <div class="col-md-6 inputGroupContainer">
		            <div class="input-group">
		              <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		              <input name="name" id="name" placeholder="Name" class="form-control"  type="text" required> 
		            </div>
		          </div>
		        </div>
				<!-- Name -->
				
				<div class="form-group">
		          <label class="col-md-3 control-label">E-Mail *</label>
		          <div class="col-md-6 inputGroupContainer">
		            <div class="input-group">
		              <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
		              <input name="email" id="email" placeholder="E-Mail Address" class="form-control"  type="email" required>
		            </div>
		          </div>
		        </div>
		        <!-- Email -->

				<div class="form-group">
		          <label class="col-md-3 control-label">Phone</label>
		          <div class="col-md-6 inputGroupContainer">
		            <div class="input-group">
		              <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
		              <input name="phone" id="phone" placeholder="(845) 555-1212" class="form-control" type="tel">
		            </div>
		          </div>
		        </div>
		        <!-- Phone -->

		        <div class="form-group">
		          <label class="col-md-3 control-label">Subject</label>
		          <div class="col-md-6 selectContainer">
		            <div class="input-group">
		              <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
		              <select name="subject" id="subject" class="form-control selectpicker" >
		                	<option value="">Select one</option>
							<option value="support">Support</option>
							<option value="sales">Sales</option>
							<option value="bugs">Report a bug</option>
							<option value="feedback">Feedback</option>
		              </select>
		            </div>
		          </div>
		        </div>
		        <!-- subject -->

		        <div class="form-group">
		          <label class="col-md-3 control-label">Message *</label>
		          <div class="col-md-6 inputGroupContainer">
		            <div class="input-group">
		              <span class="input-group-addon"><i class="glyphicon glyphicon-pencil"></i></span>
		              <textarea rows="10" class="form-control" type="text" name="message" id="message" placeholder="A project, or feedback" required></textarea>
		            </div>
		          </div>
		        </div>
		        <!-- Project description or Message -->

		        <div class="form-group">
		          <label class="col-md-3 control-label">Attachment</label>
		          <div class="col-md-6 inputGroupContainer">
		            <div class="input-group">
		              <span class="input-group-addon"><i class="glyphicon glyphicon-paperclip"></i></span>
		              <input name="attachment[]" id="attachment" class="form-control" type="file" multiple>
		            </div>
		          </div>
		        </div>
		        <!-- Upload -->

		        <div class="form-group">
		          <label class="col-md-3 control-label">Skynet Check *</label>
		          <div class="col-md-6 inputGroupContainer">
		           		<div id="g-recaptcha" class="g-recaptcha" data-sitekey="6LcaxCgUAAAAAIvzAP2rxyzQ_sb0uHnK5ZikiJoZ" data-expired-callback="recaptchaCallback"></div>
						<noscript>
							<div style="width: 302px; height: 352px;margin-bottom:20px;margin-left:100px;">
								<div style="width: 302px; height: 352px; position: relative;">
									<div style="width: 302px; height: 352px; position: absolute;">
										<!-- change YOUR_SITE_KEY with your google recaptcha key -->
										<iframe src="https://www.google.com/recaptcha/api/fallback?k=6LcaxCgUAAAAAIvzAP2rxyzQ_sb0uHnK5ZikiJoZ" style="width: 302px; height:352px; border-style: none;">
										</iframe>
									</div>
									<div style="width: 250px; height: 80px; position: absolute; border-style: none; bottom: 21px; left: 25px; margin: 0px; padding: 0px; right: 25px;">
										<textarea id="g-recaptcha-response" name="g-recaptcha-response" class="g-recaptcha-response" style="width: 250px; height: 80px; border: 1px solid #c1c1c1; margin: 0px; padding: 0px; resize: none;"></textarea>
									</div>
								</div>
							</div>
						</noscript>
					</div>
		          </div>
		        </div>
		        <!-- Google reCaptcha -->

				<!-- honeypot -->
				<div class="form-group" style="left: -9999px; position: absolute;">
					<label for="honey" class="col-sm-2 control-label">
						Please leave this field empty - we're using it to stop robots submitting the form<br>
					</label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="honey" id="honey">
					</div>
				</div>

				<div class="form-group">
		          <div class="col-md-12">
		          	<div class="progress-container"></div>
			            <button type="submit" class="btn btn-lg btn-primary center-block">Submit <span class="glyphicon glyphicon-send"></span></button>
		          </div>
		        </div>
		        <!-- Submit button -->

			</fieldset>
		</form>
	</div>
</div>