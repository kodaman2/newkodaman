<?php

error_reporting(-1);
ini_set('display_errors', 'On');

// define variables and set to empty values
$first_name = $last_name = $email = $phone = $skype = $message = $url = $success_email = "";

$errors = array();
$data   = array();

// Make sure data is not an empty string, and do any regex test
// Since we are doing JQuery validation, we shouldn't get any errors

// First name
if (empty($_POST["firstname"])) {
    $errors['firstname'] = 'Name is required!';
  } else {
    $first_name = test_input($_POST["firstname"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$first_name)) {
      $errors['firstname'] = "Only letters";
    }
}

// Last name
if (empty($_POST["lastname"])) {
    $errors['lastname'] = 'Last Name is required!';
  } else {
    $last_name = test_input($_POST["lastname"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$last_name)) {
      $errors['lastname'] = "Only letters";
    }
}

// // Email
// if (empty($_POST["email"])) {
//     $errors['email'] = "Email is required";
//   } else {
//     $email = test_input($_POST["email"]);
//     // check if e-mail address is well-formed
//     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
//       $errors['email'] = "Invalid email format"; 
//     }
// }

// // Phone
// if (empty($_POST["phone"])) {
//     $errors['phone'] = "Phone is required";
//   } else {
//     $phone = test_input($_POST["phone"]);
//     // check if phone is well-formed
//     if (!preg_match("/^(\d[\s-]?)?[\(\[\s-]{0,2}?\d{3}[\)\]\s-]{0,2}?\d{3}[\s-]?\d{4}$/i",$phone)) {
//       $errors['phone'] = "Invalid phone number"; 
//     }
// }

// // Skype
// if (empty($_POST["skype"])) {
//     $errors['skype'] = "Skype is required";
//   } else {
//     $skype = test_input($_POST["skype"]);
//     // check if user id is well-formed
//     if (!preg_match("/^[a-z0-9_.-]{3,15}$/",$skype)) {
//       $errors['skype'] = "Invalid user id";
//     }
// }

// City

// State

// Url
// if($_POST["url"]){
// 	$url = test_input($_POST["url"]);
// 	// check if URL is good
// 	if (!preg_match("/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/",$url)) {
//       $errors['url'] = "Invalid url";
//     }
// }

// Hosting: YES || NO

// Comment

if (!empty($errors)){
	  $data['success'] = false;
    $data['errors']  = $errors;
    $first_name = $last_name = $email = $phone = $skype = $url = '';
} else {
	// if there are no errors process our form, then return a message

        // DO ALL YOUR FORM PROCESSING HERE
        // THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)
        // Email process goes here or database processing.
		if (empty($errors)){
	      $message_body = '';
	      unset($_POST['submit']);
	      foreach ($_POST as $key => $value){
	          $message_body .=  "$key: $value\n";
	          print_r($message_body);
	      }
      
	      $to = 'fernando@kodaman.tech';
	      $subject = 'Contact Form Submit';
	      if (mail($to, $subject, $message_body)){
	          $success_email = "Message sent, thank you for contacting us!";
	          $first_name = $last_name = $email = $phone = $skype = $url = '';
	      }
  		}

        // show a message of success and provide a true success variable
        $data['success'] = true;
        $data['message'] = "Success!";

}

// return all our data to an AJAX call
echo json_encode($data);

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>
