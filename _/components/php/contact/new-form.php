<?php include "contact-process.php"; ?>
  <script src="../../js/jquery.js"></script>
   <script src="../../js/bootstrapvalidator.js"></script>
    <script src="../../../js/myscript-min.js"></script>

<link href="form-style.css" rel="stylesheet">

<div class="container">  
  <form id="contact" action="<?php $_SERVER['PHP_SELF'];?>" method="post">
    <h3>Quick Contact</h3>
    <h4>Contact us today, and get reply with in 24 hours!</h4>
    <fieldset>
      <input placeholder="Your name" type="text" name="first_name" tabindex="1" autofocus>
    </fieldset>
    <fieldset>
      <input placeholder="Your Email Address" type="text" name="email" tabindex="2" >
    </fieldset>
    <fieldset>
      <input placeholder="Your Phone Number" type="text" name="phone" tabindex="3" >
    </fieldset>
    <fieldset>
      <input placeholder="Your Web Site starts with http://" type="text" name="name" tabindex="4" >
    </fieldset>
    <fieldset>
      <textarea placeholder="Type your Message Here...." type="text" name="message" tabindex="5" ></textarea>
    </fieldset>
    <fieldset>
      <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Submit</button>
    </fieldset>
    <div class="sucess">
      <?php $sucess; ?>
    </div>
  </form> 
</div>