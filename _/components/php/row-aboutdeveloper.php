<div class="row content clearfix" id="aboutdeveloper">
	<div class="col-lg-12">
		<img src="images/me.jpg">
		<!-- <div>
			<img src="images/logo-full-black.png">
		</div> -->
		<h2>Welcome to Kodaman</h2>
		<p>I am a developer with a varied skillset. I enjoy working in games, apps, and web development.
		My skills include programming languages such as Java, C#, Javascript, Obj-C, Swift, Kotlin, and many more. I enjoy learning something new everyday. Other awesome skills are project management, unit testing, software design, and brewing coffee. I am really passionate about software.</p>
		<h4>Are you ready to brew something awesome together?</h4>
		<div class="btn-group" role="group" aria-label="...">
			<!-- <a href="about.php" class="btn btn-default" role="button">More about me</a> -->
			<a href="#widget-contact" class="btn btn-primary" role="button">Let's talk</a>
		</div>
	</div>
</div>