<aside class="register">
	<h2>Newsletter Registration</h2>
	<p>Fill our registration letter form and we'll email you our monthly newsletter. Then, get ready for the best conference in contemporary art.</p>
	<a href="register.php" class="btn btn-danger">Register now</a></p>
</aside>
