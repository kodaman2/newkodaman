<div class="container">
  <div class="row content" id="process">
  	<h2 class="text-center">Process</h2>
    <section class="col col-md-4 clearfix">
      <h3>Project Workflow</h3>
      <ol>
        <li>Planning</li>
        <li>Research</li>
        <li>Initial Concepts</li>
        <li>Sketch, and Wireframes</li>
        <li>Approvals</li>
        <li>Refine Concepts</li>
        <li>Testing</li>
        <li>Deployment & Backups</li>
        <li>Documentation</li>
    </ol>

    <p>Each project is unique, and tailored to each specific customer. Communication is key between developer, and the customer. It is very important to deliver project within deadline, but most importantly to deliver a high quality product.</p>

    <!-- <div>
      <p><a href="process.php" class="btn btn-primary" role="button">Learn more</a></p>
    </div> -->
   
    </section>

    <section class="col col-md-8">
       <img class="img-responsive" src="images/ux-flowchart-cards.jpg">
    </section>
  	
  </div>
</div>