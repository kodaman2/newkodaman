<div class="row content">
  <nav class="navbar navbar-default">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <!-- <a class="navbar-brand" href="#">Kodaman</a> -->
        <a href="index.php" class="navbar-left"><img src="images/brand-white_20.png"></a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
          <li><a href="about.php">About</a></li>
          <!-- <li><a href="services.php">Services</a></li> -->
          <!-- <li><a href="process.php">Process</a></li> -->
          <li><a href="contact.php">Contact</a></li>
          <li><a href="portfolio.php">Portfolio</a></li>
          <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Portfolio <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="portfolio.php">All projects</a></li>
              <li role="separator" class="divider"></li>
              <li><a id="web-button" href="portfolio.php">Websites</a></li>
              <li><a href="#">Mobile Apps</a></li>
              <li><a href="#">Games</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">Desktop Apps</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">Others</a></li>
            </ul>
          </li> -->
        </ul>
        <!-- Social media links -->
        <!-- <ul class="nav navbar-nav navbar-right">
          <li><a href="#">Link</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li role="separator" class="divider"></li>
              <li><a href="#">Separated link</a></li>
            </ul>
          </li>
        </ul> -->
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
</div>

        <!-- Modal Has nothing to do with header -->
        <section id="modal" class="modal fade">
          <div class="modal-body">
            <img id="modalimage" src="" alt="Modal Photo">
          </div>
        </section>