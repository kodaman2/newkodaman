<div class="col-sm-6 col-md-4 mobile-item item">
	<div class="thumbnail">
		<img class="img-responsive" src="images/portfolio/Pizza-Demo-02.png" alt="...">
		<div class="caption">
			<h3>Pizza App UX Concept</h3>
			<p>UX Concept of a pizza app. From Loading screen to payment. And explores all the possible scenarios a user can take, and options to avoid future typing....</p>
			<p><a href="pizza-project.php" class="btn btn-primary" role="button">Project page</a></p>
		</div>
	</div>
</div>