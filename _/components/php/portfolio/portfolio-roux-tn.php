<div class="col-sm-6 col-md-4 web-item item">
        <div class="thumbnail">
          <img class="img-responsive" src="images/portfolio/Roux-Demo.jpg" alt="...">
          <div class="caption">
            <h3>Roux Academy</h3>
            <p>Website built with bootstrap 3. The project contains many aspects of modern responsive websites. It has a modular structure to easily change components...</p>
            <p><a href="roux-project.php" class="btn btn-primary" role="button">Project page</a></p>
          </div>
        </div>
</div>