<div class="col-sm-6 col-md-4 game-item item">
        <div class="thumbnail">
          <img class="img-responsive" src="images/portfolio/SquarePatrol-Demo.jpg" alt="...">
          <div class="caption">
            <h3>Square Patrol</h3>
            <p>Square Patrol was built with Unity3D game engine. It has a minecraft feel with its blocky walls. It is scripted in C#, and contains several levels...</p>
            <p><a href="squarepatrol-project.php" class="btn btn-primary" role="button">Project page</a></p>
          </div>
        </div>
</div>